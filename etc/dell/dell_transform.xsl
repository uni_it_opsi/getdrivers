<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:om="openmanage/cm/dm">
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
	<xsl:strip-space elements="driver"/>
	<xsl:template match="/om:DriverPackManifest">
		<info>
			<xsl:variable name="DriverCount" select="count(om:DriverPackage)"/>
			<xsl:for-each select="om:DriverPackage">
				<driver>
					<xsl:attribute name="name"><xsl:value-of select="normalize-space(om:Name/om:Display)"/>
		</xsl:attribute>
					<xsl:attribute name="path"><xsl:value-of select="@path"/></xsl:attribute>
					<xsl:for-each select="om:SupportedSystems">
						<xsl:attribute name="sku">
							<xsl:value-of select="om:Brand/om:Model/@systemID"/>
						</xsl:attribute>
                        <xsl:attribute name="model">
							<xsl:value-of select="om:Brand/om:Model/@name"/>
						</xsl:attribute>
					</xsl:for-each>
					<xsl:for-each select="om:SupportedOperatingSystems/om:OperatingSystem">
						<os>
							<xsl:attribute name="arch">
								<xsl:value-of select="@osArch"/>
							</xsl:attribute>

							<xsl:value-of select="@osCode"/>
						</os>
					</xsl:for-each>
				</driver>
			</xsl:for-each>
			<test>
				<xsl:copy-of select="$DriverCount"/>
			</test>
		</info>
	</xsl:template>
</xsl:stylesheet>
